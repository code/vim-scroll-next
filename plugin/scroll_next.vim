"
" scroll_next.vim: Mapping to scroll a page forward until the buffer's end,
" and then to go to the :next buffer in the argument list.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_scroll_next') || &compatible || v:version < 700
  finish
endif
let loaded_scroll_next = 1

" If last line visible, go to next file, otherwise page down
nnoremap <expr> <Plug>(ScrollNext)
      \ line('w$') == line('$') ? ":\<C-U>next\<CR>" : "\<PageDown>"
