scroll\_next.vim
================

This plugin provides a mapping target that scrolls through the current buffer
with `PageDown` while the final line of the buffer is not visible in the
window, and `:next` to move to the next file in the argument list when it is.
It's therefore a lazy way to read several buffers in sequence while just
tapping one key.  The author likes to use it for reading several buffers on
limited keyboards, such as a mobile phone terminal client.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
